/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 *
 * @author angel
 */
public class dbProducto extends dbManejador implements Persistencia {

    public dbProducto() {
        super();
    }

    @Override
    public void insertar(Object object) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) object;
        String consulta = "";
        consulta = "INSERT INTO PRODUCTOS(codigo,nombre,precio,fecha,status) VALUES(?,?,?,?,?)";

        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, pro.getCodigo());
            this.sqlConsulta.setString(2, pro.getNombre());
            this.sqlConsulta.setString(4, pro.getFecha());
            this.sqlConsulta.setFloat(3, pro.getPrecio());
            this.sqlConsulta.setInt(5, pro.getStatus());
            this.sqlConsulta.executeUpdate();
            this.desconectar();
        }
    }

    @Override
    public void actualizar(Object object) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) object;
        String consulta = "";
        consulta = "UPDATE PRODUCTOS SET  nombre = ?, precio = ?, fecha = ? WHERE codigo = ? and status = 0";

        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(4, pro.getCodigo());
            this.sqlConsulta.setString(1, pro.getNombre());
            this.sqlConsulta.setString(3, pro.getFecha());
            this.sqlConsulta.setFloat(2, pro.getPrecio());
            this.sqlConsulta.executeUpdate();
            this.desconectar();
        }
    }

    @Override
    public void habilitar(Object object) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) object;
        String consulta = "";
        consulta = "UPDATE PRODUCTOS SET  status = 0 WHERE codigo = ? and status = 1";

        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, pro.getCodigo());
            this.sqlConsulta.executeUpdate();
            this.desconectar();
        }
    }

    @Override
    public void deshabilitar(Object object) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) object;
        String consulta = "";
        consulta = "UPDATE PRODUCTOS SET  status = 1 WHERE codigo = ? and status = 0";

        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, pro.getCodigo());
            this.sqlConsulta.executeUpdate();
            this.desconectar();
        }
    }

    @Override
    public boolean siExiste(String codigo) throws Exception {
        String consulta = "";
        consulta = "SELECT 1 FROM PRODUCTOS WHERE codigo = ?";

        boolean exito = false;

        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);
            registros = this.sqlConsulta.executeQuery();
            if (registros.next()) {
                exito = true;
            }
            this.desconectar();
        }
        return exito;
    }

    @Override
    public ArrayList lista() throws Exception {
        ArrayList listaProductos = new ArrayList<Productos>();
        String consulta = "SELECT * FROM PRODUCTOS WHERE status = 0 order by codigo";
        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            registros = this.sqlConsulta.executeQuery();
            while (registros.next()) {
                Productos pro = new Productos();
                pro.setIdProductos(registros.getInt("idproductos"));
                pro.setCodigo(registros.getString("codigo"));
                pro.setNombre(registros.getString("nombre"));
                pro.setPrecio(registros.getFloat("precio"));
                pro.setFecha(registros.getString("fecha"));
                pro.setStatus(registros.getInt("status"));
                listaProductos.add(pro);
            }
        }
        this.desconectar();
        return listaProductos;
    }

    @Override
    public Object buscar(String codigo) throws Exception {
        Productos pro = new Productos();
        String consulta = "";
        consulta = "SELECT * FROM PRODUCTOS WHERE codigo = ? and status = 0";

        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);
            registros = this.sqlConsulta.executeQuery();
            if (registros.next()) {
                pro.setIdProductos(registros.getInt("idproductos"));
                pro.setCodigo(registros.getString("codigo"));
                pro.setNombre(registros.getString("nombre"));
                pro.setPrecio(registros.getFloat("precio"));
                pro.setFecha(registros.getString("fecha"));
                pro.setStatus(registros.getInt("status"));
            }
            this.desconectar();
        }
        return pro;

    }

}
