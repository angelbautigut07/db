/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.awt.event.ActionEvent;
import Vista.jintProductos;
import Modelo.*;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.sql.SQLException;
import com.toedter.calendar.JDateChooser;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author angel
 */
public class Controlador implements ActionListener {

    private jintProductos vista;
    private dbProducto db;
    private boolean EsActualizar;

    public Controlador(jintProductos vista, dbProducto db) {
        this.vista = vista;
        this.db = db;

        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnBuscar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnDeshabilitar.addActionListener(this);
        vista.btnHabilitar.addActionListener(this);
        this.deshabilitar();
        vista.lista.getTableHeader().setReorderingAllowed(false);
    }

    public void limpiar() {
        vista.txtCodigo.setText("");
        vista.txtNombre.setText("");
        vista.txtPrecio.setText("");
        vista.dtfFecha.setDate(null);
    }

    public void cerrar() {
        int res = JOptionPane.showConfirmDialog(vista, "¿Desea cerrar el sistema?", "Productos", JOptionPane.YES_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (res == JOptionPane.YES_OPTION) {
            vista.dispose();
        }

    }

    public void habilitar() {
        vista.txtCodigo.setEnabled(true);
        vista.txtNombre.setEnabled(true);
        vista.txtPrecio.setEnabled(true);
        vista.btnBuscar.setEnabled(true);
        vista.btnCancelar.setEnabled(true);
        vista.btnGuardar.setEnabled(true);
        vista.btnLimpiar.setEnabled(true);
        vista.btnNuevo.setEnabled(true);
        vista.dtfFecha.setEnabled(true);
        vista.btnHabilitar.setEnabled(true);
    }

    public void deshabilitar() {
        vista.txtCodigo.setEnabled(!true);
        vista.txtNombre.setEnabled(!true);
        vista.txtPrecio.setEnabled(!true);
        vista.btnBuscar.setEnabled(!true);
        vista.btnCancelar.setEnabled(!true);
        vista.btnGuardar.setEnabled(!true);
        vista.btnLimpiar.setEnabled(!true);
        vista.btnDeshabilitar.setEnabled(!true);
        vista.dtfFecha.setEnabled(!true);
        vista.btnHabilitar.setEnabled(!true);
        vista.lista.setEnabled(!true);
    }

    public boolean validar() {
        boolean exito = true;
        if (vista.txtCodigo.getText().equals("") || vista.txtNombre.getText().equals("") || vista.txtPrecio.getText().equals("") ||vista.dtfFecha==null) {
            exito = false;
        }

        return exito;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.btnLimpiar) {
            this.limpiar();
        }
        if (e.getSource() == vista.btnCancelar) {
            this.limpiar();
            this.deshabilitar();
        }
        if (e.getSource() == vista.btnCerrar) {
            this.cerrar();
        }
        if (e.getSource() == vista.btnNuevo) {
            this.habilitar();
            this.EsActualizar = false;
        }
        if (e.getSource() == vista.btnGuardar) {
            if (this.validar()) {
                Productos pro = new Productos();
                pro.setCodigo(vista.txtCodigo.getText());
                pro.setNombre(vista.txtNombre.getText());
                pro.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                pro.setStatus(0);
                pro.setFecha(this.convertirAñoMesDia(vista.dtfFecha.getDate()));

                try {
                    if (this.EsActualizar){
                         db.actualizar(pro);
                            JOptionPane.showMessageDialog(vista, "Se actualizo con exito el producto");
                            this.limpiar();
                            this.deshabilitar();
                            this.ActualizarTabla(db.lista());
                    } else if (!db.siExiste(pro.getCodigo())) {
                            db.insertar(pro);
                            JOptionPane.showMessageDialog(vista, "Se agrego con exito el producto");
                            this.limpiar();
                            this.deshabilitar();
                            this.ActualizarTabla(db.lista());
                    } else JOptionPane.showMessageDialog(vista, "Ya existe un producto con ese codigo");
                    
                } catch (Exception ae) {
                    JOptionPane.showMessageDialog(vista, "Surgio un error: " + ae.getMessage());
                }
            } else {
                JOptionPane.showMessageDialog(vista, "Faltaron datos por capturar");
            }
        }
        if (e.getSource() == vista.btnBuscar) {
            Productos pro = new Productos();
            if (vista.txtCodigo.getText().equals("")) {
                JOptionPane.showMessageDialog(vista, "Falto capturar el codigo");
            } else {
                try {
                    pro = (Productos) db.buscar(vista.txtCodigo.getText());
                    if (pro.getIdProductos() != 0) {
                        vista.txtCodigo.setText(pro.getCodigo());
                        vista.txtNombre.setText(pro.getNombre());
                        vista.txtPrecio.setText(String.valueOf(pro.getPrecio()));
                        convertirStringDate(pro.getFecha());
                        this.EsActualizar = true;
                        vista.btnGuardar.setEnabled(true);
                        vista.btnDeshabilitar.setEnabled(true);
                    } else {
                        JOptionPane.showMessageDialog(vista, "No se encontro el producto");
                    }
                } catch (Exception ae) {
                    JOptionPane.showMessageDialog(vista, "Surgio un error: " + ae.getMessage());
                }
            }
        }
        if (e.getSource() == vista.btnDeshabilitar) {
            int opc = 0;
            opc = JOptionPane.showConfirmDialog(vista, "Desea deshabilitar el producto? ", "Producto",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

            if (opc == JOptionPane.YES_OPTION) {
                Productos pro = new Productos();
                pro.setCodigo(vista.txtCodigo.getText());
                try {
                    db.deshabilitar(pro);
                    JOptionPane.showMessageDialog(vista, "El producto fue deshabilitado ");
                    this.limpiar();
                    this.deshabilitar();
                    this.ActualizarTabla(db.lista());

                } catch (Exception i) {
                    JOptionPane.showMessageDialog(vista, "Surgio un error:  " + i.getMessage());
                }
            }

        }

        if (e.getSource() == vista.btnHabilitar) {
            int opc = 0;
            opc = JOptionPane.showConfirmDialog(vista, "Desea habilitar el producto? ", "Producto",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

            if (opc == JOptionPane.YES_OPTION) {
                Productos pro = new Productos();
                pro.setCodigo(vista.txtCodigo.getText());
                try {
                    db.habilitar(pro);
                    JOptionPane.showMessageDialog(vista, "El producto fue habilitado ");
                    this.limpiar();
                    this.deshabilitar();
                    this.ActualizarTabla(db.lista());

                } catch (Exception i) {
                    JOptionPane.showMessageDialog(vista, "Surgio un error:  " + i.getMessage());
                }
            }

        }
    }

    public void iniciarVista() {
        vista.setTitle("Productos");
        vista.resize(853, 551);
        vista.setVisible(true);
        try {
            this.ActualizarTabla(db.lista());
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(vista, "Surgio un error: " + ex.getMessage());
        }
    }

    public String convertirAñoMesDia(java.util.Date fecha) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(fecha);
    }

    public void convertirStringDate(String fecha) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = dateFormat.parse(fecha);
            vista.dtfFecha.setDate(date);
        } catch (ParseException e) {
            System.err.println(e.getMessage());
        }
    }
    
    public void ActualizarTabla(ArrayList<Productos> arr){
        String campos [] = {"idproductos","codigo","nombre","precio","fecha"};
        String [][] datos = new String[arr.size()][5];
        int renglon = 0;
        for(Productos registro : arr){
            datos[renglon][0] = String.valueOf(registro.getIdProductos());
            datos[renglon][1] = registro.getCodigo();
            datos[renglon][2] = registro.getNombre();
            datos[renglon][3] = String.valueOf(registro.getPrecio());
            datos[renglon][4] = registro.getFecha();
            renglon++;
        }
        DefaultTableModel tb = new DefaultTableModel (datos,campos);
        vista.lista.setModel(tb);
    }
}

    
